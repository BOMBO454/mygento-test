import {Formik} from 'formik';
import Input from "@/components/input/Input";
import InputFile from "@/components/inputFile/InputFile";
import Button from "@/components/button/Button";
import InputRadio from "@/components/inputRadio/InputRadio";
import InputCheckbox from "@/components/inputCheckbox/InputCheckbox";
import css from '../styles/pages.module.scss'
import Fieldset from "@/components/fieldset/Fieldset";
import profileScheme from "../constants/validationSchemas/profileScheme";
import {useState} from "react";
import Modal from 'react-bootstrap-modal';

export default function Home() {
  const [modalOpen, setModalOpen] = useState(false)
  const [data, setData] = useState()
  const submitHandler = (e) => {
    setModalOpen(true)
    setData(e)
  }
  return (
    <section className={css.page}>
      <h1 className={css.title}>Анкета соискателя</h1>
      <Formik
        initialValues={{
          photo: undefined
        }}
        onSubmit={submitHandler}
        validationSchema={profileScheme}
        validateOnChange={false}
        validateOnBlur={false}
      >
        {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
            setFieldValue,
            validateField,
          }) => (
          <form onSubmit={handleSubmit}>
            <Fieldset gridClassName={css.personalBlock} title="Личные данные">
              <Input placeholder="Имя" title="Имя *" error={errors.firstName} name="firstName" value={values.firstName}
                     onChange={handleChange} onBlur={() => {
                validateField('firstName')
              }}/>
              <Input placeholder="Фамилия" title="Фамилия *" error={errors.secondName} name="secondName"
                     value={values.secondName} onChange={handleChange} onBlur={() => {
                validateField('secondName')
              }}/>
              <Input placeholder="Электронная почта" title="Электронная почта *" error={errors.email} name="email"
                     value={values.email} onChange={handleChange} onBlur={() => {
                validateField('email')
              }}/>
              <InputFile
                title="Загрузить резюме"
                value={values.photo}
                name="photo"
                onChange={setFieldValue}
                errors={errors.photo}
                touched={touched["file"]}
                onBlur={() => {
                  validateField('photo')
                }}
              />
            </Fieldset>
            <Fieldset title="Пол *" error={errors.sex}>
              <InputRadio title="Мужской" error={errors.sex} name="sex"
                          onChange={handleChange} value="man" onBlur={() => {
                validateField('sex')
              }}/>
              <InputRadio title="Женский" error={errors.sex} name="sex"
                          onChange={handleChange} value="woman" onBlur={() => {
                validateField('sex')
              }}/>
            </Fieldset>

            <Fieldset className={css.githubBlock} title="Github">
              <Input className={css.githubInput} placeholder="Вставьте ссылку на Github"
                     title="Вставьте ссылку на Github"
                     error={errors.url}
                     name="url"
                     value={values.url} onChange={handleChange} onBlur={() => {
                validateField('url')
              }}/>
            </Fieldset>
            <InputCheckbox className={css.checkBox} title="* Я согласен с политикой конфиденциальности"
                           error={errors.politic} name="politic"
                           onChange={handleChange} onBlur={() => {
              validateField('politic')
            }}/>
            <Button type={"submit"} className={css.send} disabled={isSubmitting}>Отправить</Button>
          </form>
        )}
      </Formik>
      <Modal
        show={modalOpen}
        onHide={() => {
          setModalOpen(false)
        }}
      >
        Спасибо {data?.firstName}
        <p>Мы скоро свяжемся с вами</p>
        <Button onClick={()=>{setModalOpen(false)}}>Понятно</Button>
      </Modal>
    </section>
  )
}
