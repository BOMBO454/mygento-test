import * as Yup from "yup";

const FILE_SIZE = 160 * 1024;
const SUPPORTED_FORMATS = [
  "image/gif",
  "image/png",
  "image/jpg",
  "image/jpeg",
];

export default Yup.object().shape({
  firstName: Yup.string()
    .min(2, 'Слишком коротко')
    .max(50, 'Слишком длинное')
    .matches(/^[a-zA-Zа-яА-Я]+$/, 'В имени могут быть только буквы')
    .required('Укажите имя'),
  secondName: Yup.string()
    .min(2, 'Слишком коротко')
    .max(50, 'Слишком длинное')
    .matches(/^[a-zA-Zа-яА-Я]+$/, 'В фамилии могут быть только буквы')
    .required('Укажите фамилию'),
  email: Yup.string()
    .email('Неверный email')
    .required('Укажите email'),
  sex: Yup.string()
    .required('Укажите пол'),
  url: Yup.string()
    .url('Должен быть правильный URL'),
  politic: Yup.bool()
    .isTrue('Подтвердите')
    .required('Подтвердите'),
  photo: Yup.mixed()
    .test(
      "fileSize",
      "Файл слишком большой",
      value => value && value.size <= FILE_SIZE
    )
    .test(
      "fileFormat",
      "Неверный формат",
      value => value && SUPPORTED_FORMATS.includes(value.type)
    )
});
