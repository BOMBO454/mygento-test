import css from './Input.module.scss'
import PropTypes from 'prop-types'

export default function Input({placeholder, error, title, className, onChange, value, name, type, onBlur}) {
  return (
    <div className={[className, css.inputWrapper, error ? css.wrapperError : undefined].join(' ')}>
      {title && <label className={css.title} htmlFor={name}>{title}</label>}
      <input placeholder={placeholder} id={name} className={css.input} name={name} type={type} onChange={onChange}
             onBlur={onBlur} value={value}
             autoComplete={type}/>
      <span className={css.error}>{error}&nbsp;</span>
    </div>
  )
}

Input.propTypes = {
  placeholder: PropTypes.string,
  className: PropTypes.string,
  onChange: PropTypes.func,
  error: PropTypes.object,
  value: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  onBlur: PropTypes.func,
}