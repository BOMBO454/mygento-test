import css from './Fieldset.module.scss'
import PropTypes from 'prop-types'

export default function Fieldset({children, className, title, gridClassName, error}) {
  return (
    <fieldset className={[className, css.fieldset].join(' ')}>
      <span className={css.title}>{title} <span className={css.error}>{error}</span></span>
      <div className={[css.grid, gridClassName].join(' ')}>
        {children}
      </div>
    </fieldset>
  )
}

Fieldset.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
}