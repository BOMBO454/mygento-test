import css from './Button.module.scss'
import PropTypes from 'prop-types'

export default function Button({children, className, onClick}) {
  return (
    <button className={[className, css.button].join(' ')} onClick={onClick}>{children}</button>
  )
}

Button.propTypes = {
  onClick: PropTypes.func,
  className: PropTypes.string,
  children: PropTypes.node,
}