import css from './InputRadio.module.scss'
import PropTypes from 'prop-types'

export function InputRadioGroup({children, className, error, title}) {
  return (
    <fieldset className={[className, css.inputWrapperGroup].join(' ')}>
      <div className={css.inputWrapperGroupTitle}>
        <span className={css.label}>{title}</span>
        <span className={css.error}>{error}</span>
      </div>
      {children}
    </fieldset>
  )
}

export default function InputRadio({title, className, onChange, checked, name, value, onBlur}) {
  return (
    <span className={[className, css.inputWrapper].join(' ')}>
      <input
        checked={checked}
        className={css.input}
        id={name}
        name={name}
        value={value}
        type="radio"
        onChange={onChange}
        onBlur={onBlur}
      />
      {title && <label className={css.title} htmlFor={name}>{title}</label>}
    </span>
  )
}

InputRadio.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.object,
  name: PropTypes.string,
  onBlur: PropTypes.func,
}