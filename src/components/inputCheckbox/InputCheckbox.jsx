import css from './InputCheckbox.module.scss'
import PropTypes from 'prop-types'

export function InputRadioGroup({children, className, error, title}) {
  return (
    <fieldset className={[className, css.inputWrapperGroup].join(' ')}>
      <span className={css.label}>{title}</span>
      {children}
    </fieldset>
  )
}

export default function InputCheckbox({title, className, onChange, checked, name, value, onBlur, error}) {
  return (
    <div className={[className, css.inputWrapper].join(' ')}>
      <input
        checked={checked}
        className={css.input}
        id={name}
        name={name}
        value={value}
        type="checkbox"
        onChange={onChange}
        onBlur={onBlur}
      />
      {title && <label className={css.title} htmlFor={name}>{title}</label>}
      <span className={css.error}>{error}&nbsp;</span>
    </div>
  )
}

InputCheckbox.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.object,
  name: PropTypes.string,
  onBlur: PropTypes.func,
}