import css from './InputFile.module.scss'
import PropTypes from 'prop-types'
import {useRef, useState} from "react"

export default function InputFile({className,title, onChange, value, name, onBlur}) {
  const [file, setFile] = useState()
  const fileUpload = useRef()

  const handleImageChange = (e) => {
    e.preventDefault();
    let reader = new FileReader();
    let file = e.target.files[0];
    console.log('file', file)
    if (file) {
      reader.onloadend = () => {
        setFile(file)
      };
      reader.readAsDataURL(file);
      onChange(name, file);
    }
  }
  return (
    <div className={[className, css.inputWrapper].join(' ')}>
      {title && <span className={css.title} htmlFor={name}>&nbsp;</span>}
      <input
        className={css.input}
        id={name}
        name={name}
        type="file"
        onChange={handleImageChange}
        ref={fileUpload}
        onBlur={onBlur}
      />
      {file ? <button onClick={() => {
          setFile(undefined);
          fileUpload.current.value = null;
        }} className={css.file}><img className={css.iconAttach} src="media/icons/attach.svg"/>{file.name}
           <i className={css.deleteFile}><img src="media/icons/delete.svg"/></i></button> :
        <label className={css.label} htmlFor={name}>{title}</label>}
    </div>
  )
}

InputFile.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.object,
  name: PropTypes.string,
  onBlur: PropTypes.func,
}